# 3DProjector

This repo stores all the this and thats for a 3D holographic projector.

Projector one:
A 633 nm laser with collimating optics setup.

components include:

AC254-030-A-ML, a 30.0 mm lens

CCM1 BS-13M

CRM1/M

AC254-300-A-ML

<img src="./Screenshots/145Projector.png" width="300">

This setup is developed mainly for a complex modulation projection system, however
it could also be used for general projection.

A very quiet note, credit to Peter Christopher: A very quick demo based on a 
single laser source, a 400mm lens and a Jasper 4K SLM proved that the regeneration
of the replay field is achievable.

Projector two:
FourthDD display and preferably a lower resolution display.

Fourth DD display QXGA
use MetroCon to control the SLM. Note that there is a heater for use, to heat
the SLM up to 40 degress Celcius for optimum performance.

<img src="./Screenshots/4ddProjector.png" width="300">

Preferably update the control panel to match this display.

Projector three:

<img src="./Screenshots/rgbProjector.png" width="300">

## Image generation noticeboard

1.	The Jasper 4K display takes RGB 24-bit bitmap images, it also works when a R-channel only 
8-bit bitmap is displayed.
2.	For those uncertain how to do it, refer to the `MatlabCode.m` for a starter
3.	Always pay double attention to your algorithm and software before
trying to play around and question the optics hardware setup

**NOTE** Do not give too much laser power! Well actually, do give some laser power
we are currently using >20uW power.

## CMMPE 4K Control Panel

Supported Runtime version="v4.0" sku=".NETFramework, Version=v4.7.2"
Supported OS version = "Win10"

**NO GUARANTEE THAT IT WILL WORK ON YOUR COMPUTER**

TODO!!!! Update the control panel such that nxn checkerboard can be displayed.

The Jasper Control Windows app is a windows form programme created to control
the Jasper 4K display.

The Jasper 4K display acts as a secondary display.

You will see the following error message if you don't have a secondary display
connected to your computer:

![include](./Screenshots/error.PNG)

### How to use the Control Panel

This Control Panel consists of two modes, `Normal` and `Advanced`, respectively. (Future update will include a `Guru` tab)

![include](./Screenshots/panelOne.PNG)
![include](./Screenshots/panelTwo.PNG)

In `Normal`, users will be able to check the secondary display information (pixel width and height)

`Set screen No` lets you configure which screen you would like the image to be displayed,
enter a number that is valid, you will see an error message if you entered an invalid number.

The width and height textboxes display the screen boundary, you will be able to double check your display settings.

`Open file` tab lets you choose which image you would like to display onto the screen. Click `Display` to activate.
Note that the image will not be scaled, it will use it's original pixel size and the rest of the parts will be covered with black background.

`Tilt` will display something weird, if you want to properly display a good and controllable tilt pattern, go to the `Advanced` mode. 
(Future update will fix this issue)

`White` lets you display full white image.

`Black` lets you display full black image.

`Chess board` lets you display a checkerboard (color alters every pixel).

The image displayed on the secondary screen will also show up in the `Image on Display` screen for your reference.

In `Advanced` mode, things get slightly more interesting.

`Load image` gives you a chance to load a pile of images instead of one, you will be able to select which image you want to project.
Remember to load the images one by one.

The left-hand-side of this panel gives you the chance to configure the tilt pattern. Up to two tilt circles are supported (`Number of tilts`).

![include](./Screenshots/panelTwoTilt.PNG)

Use `Generate circular` to display your tilt in circles, otherwise click on `Generate full`.
You will not need to configure `Number of tilts`, `Diameter in px`, `Background` and `Centre position` if you
want to generate the tilts in full screen.

`Rotation in deg` lets you configure your tilting angle.

`Diameter in px` lets you configure how large you would like the tilt circles to be.

`Background` gives you an option to choose in between `black`,`white` and `chessboard` background, you will need to select one of them before hitting
the `generate` button.

`Centre position` is where you can configure your layout, i.e. where to put the tilt circles.

`Load Tilt` lets you choose which tilt pattern you would like to use.

Click on the `Generate` button to make it working.
