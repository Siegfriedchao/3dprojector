﻿/*
    Authored 2019. Youchao Wang.

    Update 2019. Peter Christopher.

    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

    *   Redistributions of source code must retain the above
        copyright notice, this list of conditions and the following
        disclaimer.

    *   Redistributions in binary form must reproduce the above
        copyright notice, this list of conditions and the following
        disclaimer in the documentation and/or other materials
        provided with the distribution.

    *   Neither the name of the author nor the names of its
        contributors may be used to endorse or promote products
        derived from this software without specific prior written
        permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
    FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
    COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
    ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace JasperControlWindows
{
    public partial class Form1 : Form
    {
        public bool DrawFlag = false;
        public bool PictureFlag = false;
        public int ScreenNo = 1;

        private void Button11_Click(object sender, EventArgs e)
        {
            // update the screen number, start from 1
            // TODO: add sanity check
            ScreenNo = int.Parse(textBox15.Text);
            if (ScreenNo > SystemInformation.MonitorCount)
            {
                string msg = "Please type a valid screen number";
                string cpt = "Error!";
                MessageBoxButtons button = MessageBoxButtons.OK;
                DialogResult result;
                result = MessageBox.Show(msg, cpt, button);
                if (result == DialogResult.OK)
                {
                    textBox15.Text = "2";
                    return;
                }
            }

            ScreenNo--;


            Screen[] screens = Screen.AllScreens;
            Rectangle boundary = screens[ScreenNo].Bounds;

            // obtain the secondary display panel
            int width, height;
            width = boundary.Width;
            height = boundary.Height;
            textBox2.Text = width.ToString();
            textBox3.Text = height.ToString();
        }

        public Form1()
        {
            InitializeComponent();
            
            // start from the first tab
            tabControl1.SelectTab("tabPage1");
            
            Screen[] screens = Screen.AllScreens;
            
            // sanity check
            if (SystemInformation.MonitorCount < 2)
            {
                string msg = "You need to set up the SLM to be a secondary display, programme aborting";
                string cpt = "Error!";
                MessageBoxButtons button = MessageBoxButtons.OK;
                DialogResult result;
                result = MessageBox.Show(msg, cpt, button);
                if(result == DialogResult.OK)
                {
                    this.Close();
                    Application.Exit();
                }
            }

            // programme would only execute after the sanity check
            Rectangle boundary = screens[ScreenNo].Bounds;

            // display in full screen, secondary display
            Bitmap image = new Bitmap(boundary.Width, boundary.Height);

            // obtain the secondary display panel
            int width, height;
            width = boundary.Width;
            height = boundary.Height;
            textBox2.Text = width.ToString();
            textBox3.Text = height.ToString();

        }

        public Form Show_Image(Image image, Color color)
        {
            // set up a new form to display image in full screen
            Form form = new Form
            {
                ControlBox = false,
                FormBorderStyle = FormBorderStyle.None,
                BackgroundImage = image,
                BackgroundImageLayout = ImageLayout.Center,
                BackColor = color,
                StartPosition = FormStartPosition.Manual // this must be here in order to display it on secondary screen
            };

            Screen[] screens = Screen.AllScreens;
            Rectangle boundary = screens[ScreenNo].Bounds;

            // obtain the secondary display panel
            int left, top;
            Size size;

            left = boundary.Left;
            top = boundary.Top;
            size = boundary.Size;
            form.Location = new Point(left, top);
            form.Size = size;
            form.BackColor = Color.Black;
            form.Show();
            return form;
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            // update picture flag
            PictureFlag = true;

            // update picture box
            Image image = Image.FromFile(textBox1.Text);

            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox1.Image = image;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            // Select the desired image
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Title = "Select A File";
            openDialog.Filter = "Image Files (*.png;*.jpg;*.bmp)|*.png;*.jpg;*.bmp" + "|" +
                                "All Files (*.*)|*.*";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                string FileName = openDialog.FileName;
                textBox1.Text = FileName;
            }
        }

        private void Button1_Click(object sender, EventArgs e) // display black
        {
            Close_Forms();

            Screen[] screens = Screen.AllScreens;
            Rectangle boundary = screens[ScreenNo].Bounds;

            // now to create a bitmap

            Form form = new Form
            {
                ControlBox = false,
                FormBorderStyle = FormBorderStyle.None,
                BackColor = Color.Black,
                StartPosition = FormStartPosition.Manual,
            };
            form.SetBounds(boundary.X, boundary.Y, boundary.Width, boundary.Height);

            form.Show();

            pictureBox1.Image = null;
            pictureBox1.BackgroundImage = null;
            pictureBox1.BackColor = Color.Black;
        }

        private void Button9_Click(object sender, EventArgs e) // display white
        {
            Close_Forms();

            Screen[] screens = Screen.AllScreens;
            Rectangle boundary = screens[ScreenNo].Bounds;

            // now to create a bitmap

            Form form = new Form
            {
                ControlBox = false,
                FormBorderStyle = FormBorderStyle.None,
                BackColor = Color.White,
                StartPosition = FormStartPosition.Manual,

            };
            form.SetBounds(boundary.X, boundary.Y, boundary.Width, boundary.Height);

            form.Show();

            pictureBox1.Image = null;
            pictureBox1.BackgroundImage = null;
            pictureBox1.BackColor = Color.White;
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            // close the opened forms
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "Form1")
                    Application.OpenForms[i].Close();
            }

            // display the image in full screen
            Image image = Image.FromFile(textBox1.Text);

            Screen[] screens = Screen.AllScreens;
            Rectangle boundary = screens[ScreenNo].Bounds;

            Show_Image(image, Color.Black);
        }

        public Image Chessboard_Generation(int colorOne, int colorTwo)
        {
            Screen[] screens = Screen.AllScreens;
            Rectangle boundary = screens[ScreenNo].Bounds;

            bool pixel = false;

            // display in full screen, secondary display
            Bitmap image = new Bitmap(boundary.Width, boundary.Height);

            Color newColor = Color.FromArgb(0, 0, 0);

            // Set each pixel in bmp to black and white at run time
            for (int Xcount = 0; Xcount < boundary.Width; Xcount++)
            {
                for (int Ycount = 0; Ycount < boundary.Height; Ycount++)
                {
                    if (pixel)
                    {
                        newColor = Color.FromArgb(colorOne, colorOne, colorOne);
                        image.SetPixel(Xcount, Ycount, newColor);
                    }
                    else
                    {
                        newColor = Color.FromArgb(colorTwo, colorTwo, colorTwo);
                        image.SetPixel(Xcount, Ycount, newColor);
                    }
                    // toggle boolean
                    pixel = !pixel;

                }
            }

            return image;
        }

        public Image Tilt_Generation(int colorOne, int colorTwo, int separation)
        {
            // This function generates a tilt pattern (vertical)
            Screen[] screens = Screen.AllScreens;
            Rectangle boundary = screens[0].Bounds;

            // display in full screen, secondary display
            Bitmap image = new Bitmap(boundary.Width, boundary.Height);

            Color newColor = Color.FromArgb(0, 0, 0);

            // Calculate the difference in between then create a gradient pattern
            var colorDiff = Math.Abs(colorTwo - colorOne);
            var value = (colorDiff / separation);
            double color = 0;

            double ColumnCount = 0;

            // Set each pixel at run time
            for (int Xcount = 0; Xcount < boundary.Width; Xcount++)
            {
                ColumnCount++;
                for (int Ycount = 0; Ycount < boundary.Height; Ycount++)
                {
                    if (ColumnCount >= separation)
                    {
                        ColumnCount = 0;
                    }
                    // make the entire line the same color
                    color = (colorOne + value * ColumnCount) % 255;
                    newColor = Color.FromArgb((int)color, (int)color, (int)color);
                    image.SetPixel(Xcount, Ycount, newColor);
                }
            }

            return image;
        }

        public void Close_Forms()
        {
            // close the opened forms
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "Form1")
                    Application.OpenForms[i].Close();
            }
        }

        private void Button4_Click(object sender, EventArgs e) // chess board
        {
            Close_Forms();

            Image image = Chessboard_Generation(63, 191);

            // show image on secondary display
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox1.Image = image;
            Show_Image(image, Color.Black);
        }

        private void Button5_Click(object sender, EventArgs e) // tilt
        {
            Close_Forms();

            Screen[] screens = Screen.AllScreens;
            Rectangle boundary = screens[ScreenNo].Bounds;

            // display in full screen, secondary display
            Bitmap image = new Bitmap(boundary.Width, boundary.Height);

            bool pixel = false;
            int counter = 0;

            Color newColor = Color.FromArgb(0, 0, 0);

            // Set each pixel in myBitmap to black at run time
            for (int Xcount = 0; Xcount < boundary.Width; Xcount++)
            {
                for (int Ycount = 0; Ycount < boundary.Height; Ycount++)
                {
                    if (pixel)
                    {
                        newColor = Color.FromArgb(127, 127, 127);
                        image.SetPixel(Xcount, Ycount, newColor);
                    }
                    else
                    {
                        newColor = Color.FromArgb(255, 255, 255);
                        image.SetPixel(Xcount, Ycount, newColor);
                    }
                    // toggle boolean
                    if (counter++ == 100)
                    {
                        pixel = !pixel;
                        counter = 0;
                    }

                }
            }

            Image img = new Bitmap(image);

            // show image on secondary display
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox1.Image = image;
            Show_Image(img, Color.Black);
        }

        private void Button7_Click(object sender, EventArgs e) // load image in advanced mode
        {
            // Select the desired image
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Title = "Select A File";
            openDialog.Filter = "Image Files (*.png;*.jpg;*.bmp)|*.png;*.jpg;*.bmp" + "|" +
                                "All Files (*.*)|*.*";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                string FileName = openDialog.FileName;
                comboBox2.Items.Add(FileName);
            }
        }


        private void ComboBox2_SelectedIndexChanged(object sender, EventArgs e) // display the loaded images
        {
            Close_Forms();

            Image image = Image.FromFile(comboBox2.Text);
            pictureBox2.Image = image;

            // display the image in full screen
            Show_Image(image, Color.Black);
        }

        private Image Rotate_Image(Bitmap image, float angle)
        {
            Bitmap rotatedImage = new Bitmap(image.Width, image.Height);

            using (Graphics graphics = Graphics.FromImage(rotatedImage))
            {
                // Set the rotation point to the center in the matrix
                graphics.TranslateTransform(image.Width / 2, image.Height / 2);
                // Rotate
                graphics.RotateTransform(angle);
                // Restore rotation point in the matrix
                graphics.TranslateTransform(-image.Width / 2, -image.Height / 2);
                // Draw the image on the bitmap
                graphics.DrawImage(image, new Point(0, 0));
            }

            return rotatedImage;
        }

        private void Button6_Click(object sender, EventArgs e) // generate tilt images
        {
            Close_Forms();

            int tiltCount = int.Parse(textBox5.Text);
            float rotationDeg = float.Parse(textBox4.Text);
            int diameterVal = int.Parse(textBox6.Text);
            int modeCount = comboBox1.SelectedIndex;
            int overlayX = int.Parse(textBox7.Text);
            int overlayY = int.Parse(textBox9.Text);

            // sanity check
            if (modeCount == -1)
            {
                return; // return void
            }

            int startColor = int.Parse(textBox13.Text);
            int endColor = int.Parse(textBox14.Text);
            int separation = int.Parse(textBox12.Text);

            Bitmap tiltImage = (Bitmap)Tilt_Generation(startColor, endColor, separation);

            Image tilt = Circular_Image(tiltImage, 0, 0, diameterVal);

            if (rotationDeg != 0)
            {
                tilt = Rotate_Image((Bitmap)tilt, rotationDeg);
            }

            Bitmap baseImage;
            Bitmap overlayImage;
            Color backcolor;

            // set up the background color, seems not working right now
            if (modeCount == 0)
            {
                backcolor = Color.Black;
                baseImage = (Bitmap)Chessboard_Generation(0, 0);
            }
            else if (modeCount == 1)
            {
                backcolor = Color.White;
                baseImage = (Bitmap)Chessboard_Generation(255, 255);
            }
            else
            {
                backcolor = Color.Black;
                baseImage = (Bitmap)Chessboard_Generation(63, 191);
            }

            overlayImage = (Bitmap)tilt;

            var FinalImage = new Bitmap(baseImage.Width, baseImage.Height);
            var graphics = Graphics.FromImage(FinalImage);
            graphics.CompositingMode = CompositingMode.SourceOver;

            graphics.DrawImage(baseImage, 0, 0);
            graphics.DrawImage(overlayImage, overlayX, overlayY);

            if (tiltCount > 1)
            {
                overlayX = int.Parse(textBox8.Text);
                overlayY = int.Parse(textBox10.Text);
                graphics.DrawImage(overlayImage, overlayX, overlayY);
            }

            pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox2.Image = FinalImage;
            Show_Image(FinalImage, backcolor);
        }

        private Image Circular_Image(Bitmap image, int circleUpperLeftX, int circleUpperLeftY, int circleDiameter)
        {
            // Credit to Chris Haas
            // This function trims the image into ellipse.
            Bitmap SourceImage = image;
            Rectangle CropRect = new Rectangle(circleUpperLeftX, circleUpperLeftY, circleDiameter, circleDiameter);

            Bitmap CroppedImage = SourceImage.Clone(CropRect, SourceImage.PixelFormat);
            TextureBrush TB = new TextureBrush(CroppedImage);

            Bitmap FinalImage = new Bitmap(circleDiameter, circleDiameter);

            Graphics graphics = Graphics.FromImage(FinalImage);
            graphics.FillEllipse(TB, 0, 0, circleDiameter, circleDiameter);

            return FinalImage;
        }


        private void Button10_Click(object sender, EventArgs e)
        {
            Close_Forms();

            float rotationDeg = float.Parse(textBox4.Text);
            int diameterVal = int.Parse(textBox6.Text);
            int modeCount = comboBox1.SelectedIndex;
            int overlayX = int.Parse(textBox7.Text);
            int overlayY = int.Parse(textBox9.Text);


            // sanity check
            if (modeCount == -1)
            {
                return; // return void
            }

            int startColor = int.Parse(textBox13.Text);
            int endColor = int.Parse(textBox14.Text);
            int separation = int.Parse(textBox12.Text);

            Bitmap tiltImage = (Bitmap)Tilt_Generation(startColor, endColor, separation);
            Image tilt;

            if (rotationDeg != 0)
            {
                tilt = Rotate_Image(tiltImage, rotationDeg);
            }
            else
            {
                tilt = tiltImage;
            }

            pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox2.Image = tilt;
            Show_Image(tilt, Color.Black);
        }

        // TODO: Make a tilt pattern
        // TODO: Add sanity checks
        // TODO: I am too lazy to remove the unused bits

        private void TabPage1_Click(object sender, EventArgs e)
        {
            // do nothing
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            // do nothing
        }

        private void Label4_Click(object sender, EventArgs e)
        {
            // do nothing
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void TextBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void TextBox10_TextChanged(object sender, EventArgs e)
        {

        }

        private void TextBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void TextBox7_TextChanged(object sender, EventArgs e)
        {

        }
        private void TabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void TextBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void Label5_Click(object sender, EventArgs e)
        {

        }
    }
}
