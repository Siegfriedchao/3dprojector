﻿/*
    Authored 2020. Youchao Wang.

    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

    *   Redistributions of source code must retain the above
        copyright notice, this list of conditions and the following
        disclaimer.

    *   Redistributions in binary form must reproduce the above
        copyright notice, this list of conditions and the following
        disclaimer in the documentation and/or other materials
        provided with the distribution.

    *   Neither the name of the author nor the names of its
        contributors may be used to endorse or promote products
        derived from this software without specific prior written
        permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
    FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
    COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
    ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace MultipleDisplayControlWindows
{
    public partial class Form1 : Form
    {
        public int ScreenNo = 1;

        public Form1()
        {
            InitializeComponent();
        }

        public Form Show_Image(Image image, Color color)
        {
            // set up a new form to display image in full screen
            Form form = new Form
            {
                ControlBox = false,
                FormBorderStyle = FormBorderStyle.None,
                BackgroundImage = image,
                BackgroundImageLayout = ImageLayout.Center,
                BackColor = color,
                StartPosition = FormStartPosition.Manual // this must be here in order to display it on secondary screen
            };

            Screen[] screens = Screen.AllScreens;
            Rectangle boundary = screens[ScreenNo].Bounds;

            // obtain the secondary display panel
            int left, top;
            Size size;

            left = boundary.Left;
            top = boundary.Top;
            size = boundary.Size;
            form.Location = new Point(left, top);
            form.Size = size;
            form.BackColor = Color.Black;
            form.Show();
            return form;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            // TODO: add sanity check
            int ScreenNo = 0;

            Screen[] screens = Screen.AllScreens;
            Rectangle boundary = screens[ScreenNo].Bounds;

            // obtain the display panel
            int width, height;
            width = boundary.Width;
            height = boundary.Height;
            textBox4.Text = width.ToString();
            textBox7.Text = height.ToString();
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            // TODO: add sanity check
            int ScreenNo = 1;

            Screen[] screens = Screen.AllScreens;
            Rectangle boundary = screens[ScreenNo].Bounds;

            // obtain the display panel
            int width, height;
            width = boundary.Width;
            height = boundary.Height;
            textBox5.Text = width.ToString();
            textBox8.Text = height.ToString();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            // TODO: add sanity check
            int ScreenNo = 2;

            Screen[] screens = Screen.AllScreens;
            Rectangle boundary = screens[ScreenNo].Bounds;

            // obtain the display panel
            int width, height;
            width = boundary.Width;
            height = boundary.Height;
            textBox6.Text = width.ToString();
            textBox9.Text = height.ToString();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            // Select the desired image
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Title = "Select A File";
            openDialog.Filter = "Image Files (*.png;*.jpg;*.bmp)|*.png;*.jpg;*.bmp" + "|" +
                                "All Files (*.*)|*.*";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                string FileName = openDialog.FileName;
                textBox1.Text = FileName;
            }
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            // Select the desired image
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Title = "Select A File";
            openDialog.Filter = "Image Files (*.png;*.jpg;*.bmp)|*.png;*.jpg;*.bmp" + "|" +
                                "All Files (*.*)|*.*";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                string FileName = openDialog.FileName;
                textBox2.Text = FileName;
            }
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            // Select the desired image
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Title = "Select A File";
            openDialog.Filter = "Image Files (*.png;*.jpg;*.bmp)|*.png;*.jpg;*.bmp" + "|" +
                                "All Files (*.*)|*.*";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                string FileName = openDialog.FileName;
                textBox3.Text = FileName;
            }
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            // close the opened forms
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "Form1")
                    Application.OpenForms[i].Close();
            }

            // display the image in full screen
            Image image = Image.FromFile(textBox1.Text);

            Screen[] screens = Screen.AllScreens;
            Rectangle boundary = screens[0].Bounds;
            ScreenNo = 0;
            Show_Image(image, Color.Black);
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            // close the opened forms
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "Form1")
                    Application.OpenForms[i].Close();
            }

            // display the image in full screen
            Image image = Image.FromFile(textBox2.Text);

            Screen[] screens = Screen.AllScreens;
            Rectangle boundary = screens[1].Bounds;
            ScreenNo = 1;
            Show_Image(image, Color.Black);
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            // close the opened forms
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "Form1")
                    Application.OpenForms[i].Close();
            }

            // display the image in full screen
            Image image = Image.FromFile(textBox3.Text);

            Screen[] screens = Screen.AllScreens;
            Rectangle boundary = screens[2].Bounds;
            ScreenNo = 2;
            Show_Image(image, Color.Black);
        }
    }
}
