% Authored 2019. Peter J Christopher.
% Modified 2019. Daoming Dong, Youchao Wang


% Code for generating 24 subframes for an image and for encoding them for
% the 4k Jasper SLMs

% Close all the open figures
close all;

% Clear the currently defined variables
clear;

% Read in the target image and convert to greyscale
target = complex(double(imread('jasper5.png')));
%target = complex(double(rgb2gray(imread('jasper.png'))));

% Calculate resolution factor
size = size(target);
n=sqrt(size(1)*size(2));

% Rescale the target. Mainly due to Matlab weirdness.
target = target / sqrt(mean(mean(abs(target).^2)));

% The initial replay field is just the target image
replay = target;

% Randomise the phase of the replay field because the eye is phase insensitive
% and it reduces edge enhancement
replay = replay.*exp(2.*pi.*1i.*rand(size(1),size(2)));

% Do the algorithm 10 times
for i=1:5
    % 1) Back project current replay field 
    diffraction=ifftshift(ifft2(ifftshift(replay)))*n;
    
    % 2) Apply the SLM constraints - ie Modulate or Quantise
    diffraction=exp(1i*angle(diffraction));
    
    % 3) Forward project the current SLM values
    replay=fftshift(fft2(fftshift(diffraction)))/n;
    
    % 4) Apply replay field constraints - i.e Impose target magnitude
    replay=abs(target).*exp(1i*angle(replay));
end

% Run steps 1-3 again
diffraction=ifftshift(ifft2(ifftshift(replay)))*n;
diffraction=exp(1i*angle(diffraction));
replay=fftshift(fft2(fftshift(diffraction)))/n;

image=(angle(diffraction)+pi)/(2*pi);

% Modify the following screen No to use the SLM as the secondary display
screenNo = 5;
positions=get(0, 'MonitorPositions');
[x,y]=meshgrid(1:positions(screenNo,3),1:positions(screenNo,4));
%image=mod((x+y)/10,1);

% Output image
fig=figure('WindowState', 'fullscreen', 'MenuBar', 'none', 'ToolBar', 'none', 'Position', positions(screenNo,:));
ax = axes('Parent',fig,'Units','pixels','Position',[0 0 positions(screenNo,3:4)]);
% hImshow = imshow(image, 'Parent', ax); % either uncomment this or line 67 to display the image

% normalize the image to [0 255], you do not strictly need this
todisplay = im2uint8(image - min(image(:)));
% hImshow = imshow(todisplay, 'Parent', ax);

% To test the channels R, G and B
channelR = todisplay;
% channelR = zeros(2160, 3840);
channelG = todisplay;
% channelG = zeros(2160, 3840);
channelB = todisplay;
% channelB = zeros(2160, 3840);

imageRGB(:,:,1) = channelR;
imageRGB(:,:,2) = channelG;
imageRGB(:,:,3) = channelB;

imwrite(imageRGB, 'test4kBOnly.bmp', 'bmp'); % writes the RGB 24-bit into a bmp file

% Below is for debug use, as this duplicates the lines above
% screenNo = 2;
% 
% % Output image
% fig=figure('WindowState', 'fullscreen', 'MenuBar', 'none', 'ToolBar', 'none', 'Position', positions(screenNo,:));
% ax = axes('Parent',fig,'Units','pixels','Position',[0 0 positions(screenNo,3:4)]);
% hImshow = imshow(image,'Parent',ax);
% %  

